//
//  ViewController.swift
//  findMeDemo
//
//  Created by Student on 12/5/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet weak var distanceFrom: UILabel!
    @IBOutlet weak var latPicker: UIPickerView!
    @IBOutlet weak var longPicker: UIPickerView!
    var locationManager : CLLocationManager = CLLocationManager()
    var location : CLLocation = CLLocation()
    var latData: [Int] = [Int](-90...90)
    var longData: [Int] = [Int](-180...180)
    var minuteSecond: [Int] = [Int](0...59)
    var degreeLat : Int = 0
    var minuteLat : Int = 0
    var secondLat : Int = 0
    var degreeLong : Int = 0
    var minuteLong : Int = 0
    var secondLong : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        latPicker.delegate = self
        latPicker.dataSource = self
        longPicker.delegate = self
        longPicker.dataSource = self
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == latPicker {
            if component == 0 {
                return latData.count
            }
            else {
                return minuteSecond.count
            }
        }
        else {
            if component == 0 {
                return longData.count
            }
            else {
                return minuteSecond.count
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent comp: Int) -> String? {
        if pickerView == latPicker {
            if comp == 0 {
                return String(latData[row])
            }
            else {
                return String(minuteSecond[row])
            }
        }
        else {
            if comp == 0 {
                return String(longData[row])
            }
            else {
                return String(minuteSecond[row])
            }
        }

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations[0] as CLLocation
    }
    
    @IBAction func calculateButton(_ sender: UIButton) {
        degreeLat = latData[latPicker.selectedRow(inComponent: 0)]
        minuteLat = minuteSecond[latPicker.selectedRow(inComponent: 1)]
        secondLat = minuteSecond[latPicker.selectedRow(inComponent: 2)]
        degreeLong = longData[longPicker.selectedRow(inComponent: 0)]
        minuteLong = minuteSecond[longPicker.selectedRow(inComponent: 1)]
        secondLong = minuteSecond[longPicker.selectedRow(inComponent: 2)]
        
        let lat : Float = Float(degreeLat) + Float(minuteLat/60) + Float(secondLat/60/60)
        let long : Float = Float(degreeLong) + Float(minuteLong/60) + Float(secondLong/60/60)
        
        let newPlace = CLLocation(latitude: Double(lat), longitude: Double(long))
        
        let distance = (location.distance(from: newPlace))/1609.344
        
        distanceFrom.text = "You are " + String(format: "%.5f", distance) + " miles from the input location.\nYou are at " + String(format: "%.5f", location.coordinate.latitude) + " lat " + String(format: "%.5f", location.coordinate.longitude) + " long."
    }
    
    
    
    
}

