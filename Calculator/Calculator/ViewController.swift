//
//  ViewController.swift
//  Calculator
//
//  Created by Student on 9/22/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    enum stateType {
        case start
        case op1w
        case op1d
        case error
        case start2
        case op2start
        case op2w
        case op2d
        case equals
    }
    
    var state:stateType = stateType.start
    var operand1:Float64 = 0.0
    var operand2:Float64 = 0.0
    var decimalPlace:Int = 10
    var operatorChosen:String = ""
    
    @IBOutlet weak var display: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func specialHandler(_ sender: UIButton) {
        switch sender.currentTitle {
        case "Pi":
            switch state {
            case stateType.start,stateType.op1w,stateType.op1d,stateType.equals:
                state = stateType.op1d
                operand1 = 3.14159265359
                display.text = String(operand1)
            case stateType.start2,stateType.op2start,stateType.op2w, stateType.op2d:
                state = stateType.op2d
                operand2 = 3.14159265359
                display.text = String(operand2)
            default:
                break
            }
        default:
            break
        }
    }
    
    @IBAction func negativeHandler(_ sender: UIButton) {
        switch state {
        case stateType.start,stateType.error,stateType.start2,stateType.op2start:
            break
            
        case stateType.op1w,stateType.op1d,stateType.equals:
            operand1 = -operand1
            display.text = String(operand1)
            
        case stateType.op2w, stateType.op2d:
            operand2 = -operand2
            display.text = String(operand2)
        }
    }
    
    @IBAction func clearHandler(_ sender: UIButton) {
        state = stateType.start
        operand1 = 0.0
        operand2 = 0.0
        decimalPlace = 10
        display.text = "0"
    }
    
    @IBAction func operatorHandler(_ sender: UIButton) {
        switch state {
        case stateType.start:
            print("Error: Bad input for state start")
            
        case stateType.op1w:
            if sender.currentTitle != "="
            {
                operatorChosen = sender.currentTitle!
                state = stateType.start2
                display.text = "0"
            }
            
        case stateType.op1d:
            if sender.currentTitle != "="
            {
                operatorChosen = sender.currentTitle!
                state = stateType.start2
                display.text = "0"
                decimalPlace = 10
            }
            
        case stateType.error:
            break
            
        case stateType.start2:
            if sender.currentTitle != "=" {
                operatorChosen = sender.currentTitle!
            }
            else {
                switch operatorChosen{
                    case "+":
                    operand1 = operand1 + operand2
                    case "-":
                    operand1 = operand1 - operand2
                    case "X":
                    operand1 = operand1 * operand2
                    default:
                    operand1 = operand1 / operand2
                }
                state = stateType.equals
            }
            display.text = String(operand1)

            
        case stateType.op2start, stateType.op2d, stateType.op2w:
            switch operatorChosen{
            case "+":
                operand1 = operand1 + operand2
            case "-":
                operand1 = operand1 - operand2
            case "X":
                operand1 = operand1 * operand2
            case "/":
                operand1 = operand1 / operand2
            default:
                break
            }
            if sender.currentTitle! == "="
            {
                state = stateType.equals
            }
            else{
                state = stateType.start2
            }
            display.text = String(operand1)
            
        case stateType.equals:
            if sender.currentTitle! == "="
            {
                switch operatorChosen{
                case "+":
                    operand1 = operand1 + operand2
                case "-":
                    operand1 = operand1 - operand2
                case "X":
                    operand1 = operand1 * operand2
                default:
                    operand1 = operand1 / operand2
                }
            }
            else
            {
                operatorChosen = sender.currentTitle!
                state = stateType.start2
                operand2 = 0
                decimalPlace = 10
                
            }
            display.text = String(operand1)
        }
        print(operatorChosen)
    }
    @IBAction func decimalHandler(_ sender: UIButton) {
        switch state {
        case stateType.start:
            state = stateType.op1d
            display.text = "0."
            
        case stateType.op1w:
            state = stateType.op1d
            
        case stateType.op1d:
            state = stateType.error
            print("Bad input for state op1d")
        case stateType.error:
            break
            
        case stateType.start2:
            state = stateType.op2d
            decimalPlace = 10
            display.text = "0."
            
        case stateType.op2start:
            state = stateType.op2d
            display.text = "0."
            
        case stateType.op2w:
            state = stateType.op2d
            
        case stateType.op2d:
            print("Error: Bad input for state op2d")
            state = stateType.error
            
        case stateType.equals:
            operand2 = 0
            operand1 = 0
            decimalPlace = 10
            operatorChosen = ""
            display.text = "0."
            state = stateType.op1d
        }
    }
    
    @IBAction func digitHandler(_ sender: UIButton) {
    
        switch state {
            case stateType.start:
                if (sender.currentTitle == "0") {
                    state = stateType.start
                    operand1 = Float64(sender.currentTitle!)!
                }
                else {
                    operand1 = Float64(sender.currentTitle!)!
                    state = stateType.op1w
                    display.text = String(operand1)
                }
            
            case stateType.op1w:
                operand1 = (10*operand1)+Float64(sender.currentTitle!)!
                display.text = String(operand1)
            
            case stateType.op1d:
                if operand1 >= 0 {
                    operand1 = operand1+(Float64(sender.currentTitle!)! / Float64(decimalPlace))
                }
                else {
                    operand1 = operand1-(Float64(sender.currentTitle!)! / Float64(decimalPlace))
                }
                
                decimalPlace = decimalPlace*10
                display.text = String(operand1)
            
            case stateType.error:
                break
            
            case stateType.start2:
                if sender.currentTitle != "0"
                {
                    state = stateType.op2w
                    operand2 = Float64(sender.currentTitle!)!
                    display.text = String(operand2)
                    decimalPlace = 10
                }
                else
                {
                    state = stateType.op2start
                }
            
            case stateType.op2start:
                if (sender.currentTitle == "0") {
                    state = stateType.op2start
                }
                else {
                    operand2 = Float64(sender.currentTitle!)!
                    state = stateType.op2w
                    display.text = String(operand2)
                }
            
            case stateType.op2w:
                operand2 = (10*operand2)+Float64(sender.currentTitle!)!
                display.text = String(operand2)
            
            case stateType.op2d:
                if operand2 >= 0 {
                    operand2 = operand2+(Float64(sender.currentTitle!)! / Float64(decimalPlace))
                }
                else {
                    operand2 = operand2-(Float64(sender.currentTitle!)! / Float64(decimalPlace))
                }
                decimalPlace = decimalPlace*10
                display.text = String(operand2)
            case stateType.equals:
                if sender.currentTitle != "0"
                {
                    state = stateType.op1w
                }
                else{
                    state = stateType.start
                }
                operand1 = Float64(sender.currentTitle!)!
                operand2 = 0
                decimalPlace = 10
                display.text = String(operand1)
            }
        print(operand1)
        print(operand2)
        }
    }
