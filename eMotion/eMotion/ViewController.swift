//
//  ViewController.swift
//  eMotion
//
//  Created by Student on 10/30/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController
{
    @IBOutlet weak var xDisplay: UILabel!
    @IBOutlet weak var yDisplay: UILabel!
    @IBOutlet weak var zDisplay: UILabel!
    @IBOutlet weak var maxDisplay: UILabel!
    @IBOutlet weak var spinDisplay: UILabel!
    @IBOutlet weak var rxDisplay: UILabel!
    @IBOutlet weak var ryDisplay: UILabel!
    @IBOutlet weak var rzDisplay: UILabel!
    @IBOutlet weak var mxDisplay: UILabel!
    @IBOutlet weak var myDisplay: UILabel!
    @IBOutlet weak var mzDisplay: UILabel!
    @IBOutlet weak var mmaxDisplay: UILabel!
    
    var motionManager: CMMotionManager!
    var currMax : Double! = 0.0
    var curRot : Double! = 0.0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        motionManager = CMMotionManager()
        if motionManager.isDeviceMotionAvailable
        {
            motionManager.deviceMotionUpdateInterval = 1.0/60.0
            let queue = OperationQueue.main
            motionManager.startDeviceMotionUpdates(to: queue, withHandler:  {(data, error) in
                    if let values = data
                    {
                        let accel = sqrt(Double(values.userAcceleration.x*values.userAcceleration.x) + Double(values.userAcceleration.y*values.userAcceleration.y) + Double(values.userAcceleration.z*values.userAcceleration.z))
                        let spin = sqrt((values.rotationRate.x*values.rotationRate.x)+(values.rotationRate.y*values.rotationRate.y)+(values.rotationRate.z*values.rotationRate.z))
                        let heading = sqrt((values.magneticField.field.x*values.magneticField.field.x)+(values.magneticField.field.y*values.magneticField.field.y)+(values.magneticField.field.z*values.magneticField.field.z))
                        
                        self.xDisplay.text = String(format: "%.4f", values.gravity.x+values.userAcceleration.x)
                        self.yDisplay.text = String(format: "%.4f", values.gravity.y+values.userAcceleration.y)
                        self.zDisplay.text = String(format: "%.4f", values.gravity.z+values.userAcceleration.z)
                        
                        self.currMax = (max(self.currMax,accel))
                        self.maxDisplay.text = String(format: "%.4f",self.currMax)
                        
                        self.rxDisplay.text = String(format: "%.4f",values.rotationRate.x)
                        self.ryDisplay.text = String(format: "%.4f",values.rotationRate.y)
                        self.rzDisplay.text = String(format: "%.4f",values.rotationRate.z)
                        
                        self.curRot = (max(self.curRot, spin))
                        self.spinDisplay.text = String(format: "%.4f", self.curRot)
                        
                        self.mxDisplay.text = String(format: "%.4f",values.magneticField.field.x)
                        self.myDisplay.text = String(format: "%.4f",values.magneticField.field.y)
                        self.mzDisplay.text = String(format: "%.4f",values.magneticField.field.z)
                        
                        self.mmaxDisplay.text = String(format: "%.4f",heading)
                    }
                })
        }
    }
}
