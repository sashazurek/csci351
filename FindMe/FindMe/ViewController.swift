//
//  ViewController.swift
//  FindMe
//
//  Created by Student on 11/11/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var locationDisplay: UILabel!
    @IBOutlet weak var longitudeDisplay: UILabel!
    @IBOutlet weak var Altitude: UILabel!
    var locationManager : CLLocationManager = CLLocationManager()
    var location : CLLocation = CLLocation()
    var latData: [Int] = [Int]()
    var longData: [Int] = [Int]()
    var minuteSecond: [Int] = [Int]()
    let startn = -90
    let endn = 90
    var degreeLat : Int = 0
    var minuteLat : Int = 0
    var secondLat : Int = 0
    var degreeLong : Int = 0
    var minuteLong : Int = 0
    var secondLong : Int = 0
    @IBOutlet weak var distanceFrom: UILabel!
    @IBOutlet weak var coordPicker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        latData = Array(-90...90)
        longData = Array(-180...180)
        minuteSecond = Array(
        coordPicker.delegate = self
        coordPicker.dataSource = self
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        // 181 60 60 -- latitude dimensions
        // 361 60 60 -- longitude dimensions
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 6
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return locData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent comp: Int) -> String? {
        return String(locData[row])
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        locationDisplay.text = "Latitude: " + String(format: "%.4f",userLocation.coordinate.latitude)
        longitudeDisplay.text = "Longitude: " + String(format: "%.4f",userLocation.coordinate.longitude)
        Altitude.text = "Altitude: " + String(format: "%.2f",userLocation.altitude)
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        degreeLat = locData[coordPicker.selectedRow(inComponent: 0)]
        minuteLat = locData[coordPicker.selectedRow(inComponent: 1)]
        secondLat = locData[coordPicker.selectedRow(inComponent: 2)]
        degreeLong = locData[coordPicker.selectedRow(inComponent: 3)]
        minuteLong = locData[coordPicker.selectedRow(inComponent: 4)]
        secondLong = locData[coordPicker.selectedRow(inComponent: 5)]
        
        let latitude : Float = Float(degreeLat) + Float(minuteLat/60) + Float(secondLat/60/60)
        let longitude : Float = Float(degreeLong) + Float(minuteLong/60) + Float(secondLong/60/60)
        
        print(latitude)
        print(longitude)
    }

    
    @IBAction func calculateDistance(_ sender: UIButton) {
        var inputLocation
    }
    


}

