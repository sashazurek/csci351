//
//  ViewController.swift
//  Hello World
//
//  Created by Student on 9/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBOutlet weak var changedLabel: UILabel!
    
    var state: Int = 0;
    
    @IBAction func labelChanger(_ sender: UIButton) {
        if (state == 0)
        {
            changedLabel.text = "I'm being chased"
            state = 1
        } else if (state == 1)
        {
            changedLabel.text = "by an"
            state  = 2
        } else if (state == 2)
        {
            changedLabel.text = "arborcidal maniac!"
            state = 3
        } else if (state == 3)
        {
            changedLabel.text = "Help!"
            state = 0
        }
    }
    
    @IBAction func quitButton(_ sender: UIButton) {
        print("I can't believe you've done this")
        exit(0)
    }
    
}

